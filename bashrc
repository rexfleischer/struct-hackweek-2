#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export JOB_SERVICE_LIB_BASH_INIT="$DIR/lib-bash/__init__"
export JOB_SERVICE_PROJECT_ROOT="$DIR"
export PATH="$PATH:$DIR/bin"

eval $(docker-machine env default)
