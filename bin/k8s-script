#!/bin/bash

function show_useage_and_exit {
    echo ">> a tool for running a script on a temp pod in k8s

usage:
$(basename "$0") [--pod-name] [--image] [--env <envs>] [--namespace <ns>] [--no-delete]

and execute whatever script from stdin

where:
    --pod-name        the name of the pod
    --image           the image to run
    --env             set the environments
    --namespace       set the namespace to make the pod
    --no-delete       dont delete resources after running
"
    exit 1
}

pod_name="temp-shell"
image="bash"
environments=""
namespace=""
delete_pods=true

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --pod-name)
    pod_name="$2"
    shift
    shift
    ;;
    --image)
    image="$2"
    shift
    shift
    ;;
    --env)
    environments="--env $2"
    shift
    shift
    ;;
    --namespace)
    namespace="--namespace $2"
    shift
    shift
    ;;
    --no-delete)
    delete_pods=false
    shift
    ;;
    --)
    shift
    break
    ;;
    *)
    show_useage_and_exit
    ;;
esac
done

full_name="k8s-script-$pod_name"
script_dir="./$full_name"
script_path="./$full_name/script"
script_entry="$@"
full_script=""
while read -r line; do
    full_script="$full_script
    $line"
done <<< "`cat`"

source $JOB_SERVICE_LIB_BASH_INIT
print_stage "clearing old resources related to $full_name..."
kube_events_clear --involved $full_name $namespace
kube_resource_delete configmap $full_name $namespace
kube_resource_delete pod $full_name $namespace

print_stage "creating resources to run script"
cat <<EOF | kubectl create $namespace -f -
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    heritage: k8s-script
    release: $full_name
  name: $full_name
data:
  script: |-$full_script
EOF

cat <<EOF | kubectl create $namespace -f -
apiVersion: v1
kind: Pod
metadata:
  labels:
    heritage: k8s-script
    release: $full_name
  name: $full_name
spec:
  restartPolicy: Never
  containers:
  - name: $full_name
    image: $image
    command: [ "$script_entry", "$script_path" ]
    volumeMounts:
      - name: $full_name-config-volume
        mountPath: $script_dir
  volumes:
    - name: $full_name-config-volume
      configMap:
        name: $full_name
EOF

print_stage "waiting for pod to be created"
retry_many 60 2 kube_pod_is_created $full_name $namespace

print_stage "attempting to tail logs..."
set +e
retry_many 5 1 kubectl logs $full_name $namespace -f
set -e

exit_status=`kubectl get pod $full_name $namespace --output jsonpath="{.status.containerStatuses[].state.terminated.exitCode}"`
if [ "$exit_status" == "" ]; then
  exit_status=1
fi
print_stage "result from script => $exit_status"

print_stage "pod events..."
kubectl get events $namespace | grep $full_name

if [ "$delete_pods" = true ]; then
  print_stage "cleanup..."
  kube_resource_delete configmap $full_name $namespace
  kube_resource_delete pod $full_name $namespace
  kube_events_clear --involved $full_name $namespace
fi

exit $exit_status
