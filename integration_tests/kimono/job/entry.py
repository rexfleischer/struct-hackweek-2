import argparse
import os
import time
import requests
import base64
import pprint

args_parser = argparse.ArgumentParser()
args = args_parser.parse_args()

KIMONO_STATE_NOT_COLLECTED = 1
KIMONO_STATE_QUERIED = 2
KIMONO_STATE_PROCESSING = 3
KIMONO_STATE_COMPLETE = 4
KIMONO_STATE_FAILED = 5
KIMONO_STATE_CANCELLED = 6

def get_env_or_raise(name):
    try:
        return os.environ[name]
    except:
        print("{} is a require env".format(name))
        raise

def do_request_printing(response, data):
    request = response.request
    print("====================", response.request.method)
    print(">  url:", request.url)
    print(">  headers:")
    for header in request.headers:
        print(">   -", header, "=", request.headers[header])
    if data is not None:
        print(">  body:")
        print(data)
    # print out the response
    print("<  response:", response.status_code)
    print("<  headers:")
    for header in response.headers:
        print("<   -", header, "=", response.headers[header])
    if response.text:
        print("<  body:")
        print(response.text)

def do_request(method, url, do_printing, **kwargs):
    response = requests.request(method, url, **kwargs)
    if do_printing:
        do_request_printing(response, kwargs.get("data", None))
    try:
        response.raise_for_status()
    except:
        # if an error happens, then do the printing anyways if not done already
        if not do_printing:
            do_request_printing(response, kwargs.get("data", None))
        raise
    try:
        return response.json()
    except:
        print("could not parse json... raw response")
        print(response.text)
        exit(2)

def get_oauth_token(endpoint, username, password):
    basing = base64.b64encode(str.encode("{}:{}".format(username, password))).decode()
    return do_request("POST", endpoint + "/oauth/token", True,
                      data="grant_type=client_credentials",
                      headers={
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json",
                        "Authorization": "Basic {}".format(basing)
                      })["access_token"]

def post_start_exchange(endpoint, token, course_id):
    return do_request("POST", endpoint + "/v1/exchanges?id={}".format(course_id), True,
                      data="{}",
                      headers={
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Bearer {}".format(token)
                      })

def get_exchange_status(endpoint, token, exchange_id):
    return do_request("GET", endpoint + "/v1/exchanges/{}".format(exchange_id), False,
                      headers={
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Bearer {}".format(token)
                      })

def get_exchange_errors(endpoint, token, exchange_id):
    return do_request("GET", endpoint + "/v1/exchanges/{}".format(exchange_id) + "/errors", True,
                      headers={
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Bearer {}".format(token)
                      })

def main():
    kimono_endpoint = get_env_or_raise("KIMONO_ENDPOINT")
    kimono_agent_id = get_env_or_raise("KIMONO_AGENT_ID")
    kimono_agent_secret = get_env_or_raise("KIMONO_AGENT_SECRET")
    course_id = get_env_or_raise("COURSE_ID")

    print("getting oauth token")
    oauth_token = get_oauth_token(kimono_endpoint, kimono_agent_id, kimono_agent_secret)

    print("starting the exchange")
    exchange = post_start_exchange(kimono_endpoint, oauth_token, course_id)
    exchange_id = exchange["id"]

    print("checking on exchange status")
    while True:
        time.sleep(2)
        check = get_exchange_status(kimono_endpoint, oauth_token, exchange_id)
        check_state = check.get("state", -1)

        # depending on the state lets response correctly
        if check_state == KIMONO_STATE_NOT_COLLECTED:
            print("things not collected... omg, i have not idea...?")
        elif check_state == KIMONO_STATE_QUERIED:
            print("exchange is in queued state...")
        elif check_state == KIMONO_STATE_PROCESSING:
            print("exchange is processing...")
        elif check_state == KIMONO_STATE_COMPLETE:
            print("exchange completed!")
            break
        elif check_state == KIMONO_STATE_FAILED:
            print("exchange failed... :(")
            break
        elif check_state == KIMONO_STATE_CANCELLED:
            print("exchange got cancelled... :O")
            break
        else:
            print("unknown kimono state... exiting job")
            break

    print("finished with exchange... checking for errors")
    pprint.pprint(get_exchange_errors(kimono_endpoint, oauth_token, exchange_id))


if __name__ == '__main__':
    main()
