#!/usr/bin/env bash

source $JOB_SERVICE_LIB_BASH_INIT
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
docker login $(minikube ip):30080 -u default-user -p default-pass
docker build $JOB_SERVICE_PROJECT_ROOT/jobservice --file Dockerfile.job_attempt --tag $(minikube ip):30080/job-service-images/job-hook-helper:latest
docker push $(minikube ip):30080/job-service-images/job-hook-helper:latest

set +e
kubectl delete job job-manual-test-1 -n job-exec
set -e
cat <<EOF | convert yaml json | kubectl create -f -
apiVersion: "batch/v1"
kind: Job
metadata:
  name: "job-manual-test-1"
  namespace: "job-exec"
  labels:
    fromjobservice: "true"
    job_run_id: "manual-test"
    job_stage_index: "1"
spec:
  backoffLimit: 0
  template:
    spec:
      restartPolicy: "Never"
      containers:
        - name: job
          image: "127.0.0.1:30080/job-images/job_stage_1:latest"
          env:
            - name: "SOME_JOB_1_ENV"
              value: "234"
            - name: "SOME_ENV"
              value: "123"
          volumeMounts:
            - mountPath: "/data/next"
              name: "out-volume"
        - name: sidecar-post
          image: "127.0.0.1:30080/job-service-images/job-hook-helper"
          command: [ "python3", "./job_attempt/entry.py", "post" ]
          env:
            - name: "K8S_JOB_EXEC_NAMESPACE"
              value: "job-exec"
            - name: "JOB_RUN_ID"
              value: "manual-test"
            - name: "JOB_RUN_STAGE_INDEX"
              value: "1"
            - name: "MONGO_CONNECTION_URI"
              value: "mongodb://mongodb.job-service.svc.cluster.local:27017/"
            - name: "MONGO_DATABASE"
              value: "job_service"
            - name: "S3_OBJECT_NAME"
              value: "job-manual-test-1"
          volumeMounts:
            - mountPath: "/data/out_volume"
              name: "out-volume"
      volumes:
        - name: "out-volume"
          emptyDir: {}
EOF

echo "=========================================================================================="
echo "=========================================================================================="
for ((i=0; i<=10; i++)); do
  if kubectl logs `kubectl get pods -n job-exec | grep job-manual-test-1 | awk '{print $1}'` -n job-exec job -f; then
    break
  fi
  sleep 1
done

echo "=========================================================================================="
echo "=========================================================================================="
for ((i=0; i<=10; i++)); do
  if kubectl logs `kubectl get pods -n job-exec | grep job-manual-test-1 | awk '{print $1}'` -n job-exec sidecar-post -f; then
    break
  fi
  sleep 1
done

set +e
kubectl delete job job-manual-test-2 -n job-exec
set -e
cat <<EOF | kubectl create -f -
apiVersion: "batch/v1"
kind: Job
metadata:
  name: "job-manual-test-2"
  namespace: "job-exec"
  labels:
    fromjobservice: "true"
    job_run_id: "manual-test"
    job_stage_index: "2"
spec:
  backoffLimit: 0
  template:
    spec:
      restartPolicy: "Never"
      initContainers:
        - name: sidecar-init
          image: "127.0.0.1:30080/job-service-images/job-hook-helper"
          command: [ "python3", "./job_attempt/entry.py", "init" ]
          env:
            - name: "S3_OBJECT_NAME"
              value: "job-manual-test-1"
          volumeMounts:
            - name: "in-volume"
              mountPath: "/data/in_volume"
      containers:
        - name: job
          image: "127.0.0.1:30080/job-images/job_stage_2:latest"
          env:
            - name: "SOME_JOB_2_ENV"
              value: "345"
            - name: "SOME_ENV"
              value: "123"
          volumeMounts:
            - mountPath: "/data/prev"
              name: "in-volume"
            - mountPath: "/data/next"
              name: "out-volume"
        - name: sidecar-post
          image: "127.0.0.1:30080/job-service-images/job-hook-helper"
          command: [ "python3", "./job_attempt/entry.py", "post" ]
          env:
            - name: "K8S_JOB_EXEC_NAMESPACE"
              value: "job-exec"
            - name: "JOB_RUN_ID"
              value: "manual-test"
            - name: "JOB_RUN_STAGE_INDEX"
              value: "2"
            - name: "MONGO_CONNECTION_URI"
              value: "mongodb://mongodb.job-service.svc.cluster.local:27017/"
            - name: "MONGO_DATABASE"
              value: "job_service"
            - name: "S3_OBJECT_NAME"
              value: "job-manual-test-2"
          volumeMounts:
            - mountPath: "/data/out_volume"
              name: "out-volume"
      volumes:
        - name: "in-volume"
          emptyDir: {}
        - name: "out-volume"
          emptyDir: {}
EOF

echo "=========================================================================================="
echo "=========================================================================================="
for ((i=0; i<=10; i++)); do
  if kubectl logs `kubectl get pods -n job-exec | grep job-manual-test-2 | awk '{print $1}'` -n job-exec sidecar-init -f; then
    break
  fi
  sleep 1
done

echo "=========================================================================================="
echo "=========================================================================================="
for ((i=0; i<=10; i++)); do
  if kubectl logs `kubectl get pods -n job-exec | grep job-manual-test-2 | awk '{print $1}'` -n job-exec job -f; then
    break
  fi
  sleep 1
done

echo "=========================================================================================="
echo "=========================================================================================="
for ((i=0; i<=10; i++)); do
  if kubectl logs `kubectl get pods -n job-exec | grep job-manual-test-2 | awk '{print $1}'` -n job-exec sidecar-post -f; then
    break
  fi
  sleep 1
done
