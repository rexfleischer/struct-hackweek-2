from unittest import TestCase
import argparse
import commons_test.test_runner as test_runner
import commons_test.test_util as test_util
import commons.mongo as mongo
import commons.model as model

# ===================================================================
#
# test helpers
#
# ===================================================================

def _build_input_params(spec):
    if "parameters" not in spec:
        return { }
    result = { }
    for param_spec in spec["parameters"]:
        if "type" in param_spec:
            result[param_spec["name"]] = test_util.generate_random_from_spec(param_spec)
        elif "schema" in param_spec:
            result[param_spec["name"]] = test_util.generate_random_from_spec(param_spec["schema"])
        else:
            raise Exception("idk")
    return result

def _build_expected_codes(spec):
    result = []
    for code in spec["responses"]:
        if code < 500:
            result.append(code)
    return result

def _sub_path_params(path, params):
    return path.format(**params)

def _get_body_param_schema(spec):
    for param in spec["parameters"]:
        if param["in"] == "body":
            model_name = param["schema"]["$ref"].split("/")[-1]
            return model.model_schemas[model_name]
    import pprint
    pprint.pprint(spec)
    raise Exception("not found")

def _create_generator(type_name):
    def generator():
        result = test_util.generate_random_type(type_name)
        if type_name == "Event":
            mongo.get_events_dao().insert(result)
        elif type_name == "JobDefinition":
            mongo.get_job_definition_dao().insert(result)
        elif type_name == "JobRunAttempt":
            mongo.get_job_run_attempt_dao().insert(result)
        elif type_name == "JobRun":
            mongo.get_job_run_dao().insert(result)
        else:
            raise Exception("unknown type name: " + type_name)
        return result

    return generator

def _create_convert(spec):
    convert_schema = model.model_schemas[spec["responses"][200]["schema"]["items"]["$ref"].split("/")[-1]]
    convert_props = []
    for key in convert_schema["properties"]:
        convert_props.append(key)

    def convert(full_model):
        convert_result = { }
        for prop_name in convert_props:
            convert_result[prop_name] = full_model[prop_name]
        return convert_result

    return convert

# ===================================================================
#
# test execs
#
# ===================================================================

def test_simple_get_request(asserts: TestCase, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    test_util.do_test_request("GET", full_path, expected_codes)

def test_simple_get_by_id(asserts: TestCase, path, spec):
    generator = _create_generator(spec["responses"][200]["schema"]["$ref"].split("/")[-1])
    insert = generator()
    id_key = spec["parameters"][0]["name"]
    full_path = _sub_path_params(path, { id_key: insert["_id"] })
    check = test_util.do_test_request("GET", full_path, 200)
    asserts.assertEqual(insert, check)

def test_simple_delete_request(asserts: TestCase, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    test_util.do_test_request("DELETE", full_path, expected_codes)

def test_simple_delete_by_id(asserts: TestCase, path, spec):
    generator = _create_generator(spec["x-test-delete-by-id"])
    insert = generator()
    id_key = spec["parameters"][0]["name"]
    full_path = _sub_path_params(path, { id_key: insert["_id"] })
    check = test_util.do_test_request("DELETE", full_path, 200)
    asserts.assertEqual({ "deleted": True }, check)
    check = test_util.do_test_request("DELETE", full_path, 200)
    asserts.assertEqual({ "deleted": False }, check)

def test_simple_put_request(asserts: TestCase, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    test_util.do_test_request("PUT", full_path, expected_codes, body=params["body"])

def test_simple_post_request(asserts: TestCase, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    test_util.do_test_request("POST", full_path, expected_codes, body=params["body"])

def test_double_insert_fails(asserts: TestCase, method, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    test_util.do_test_request(method, full_path, 200, body=params["body"])
    test_util.do_test_request(method, full_path, 400, body=params["body"])

def test_empty_body(asserts: TestCase, method, path, spec):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    test_util.do_test_request(method, full_path, expected_codes, body={ })

def test_missing_field_in_body(asserts: TestCase, method, path, spec, missing):
    params = _build_input_params(spec)
    full_path = _sub_path_params(path, params)
    expected_codes = _build_expected_codes(spec)
    body = params["body"]
    del body[missing]
    test_util.do_test_request(method, full_path, expected_codes, body=body)

def test_search_paging(asserts: TestCase, path, spec):
    generator = _create_generator(spec["x-test-search-seed"])
    inserts = []
    for index in range(0, 12):
        inserts.append(generator())
    result_convertion = list(map(_create_convert(spec), inserts))

    result_list = test_util.do_test_request("POST", path, 200, body={ "page_size": 5, "page": 0 })
    asserts.assertEqual(5, len(result_list))
    asserts.assertTrue(result_list[0] in result_convertion)
    result_convertion.remove(result_list[0])
    asserts.assertTrue(result_list[1] in result_convertion)
    result_convertion.remove(result_list[1])
    asserts.assertTrue(result_list[2] in result_convertion)
    result_convertion.remove(result_list[2])
    asserts.assertTrue(result_list[3] in result_convertion)
    result_convertion.remove(result_list[3])
    asserts.assertTrue(result_list[4] in result_convertion)
    result_convertion.remove(result_list[4])
    result_list = test_util.do_test_request("POST", path, 200, body={ "page_size": 5, "page": 1 })
    asserts.assertEqual(5, len(result_list))
    asserts.assertTrue(result_list[0] in result_convertion)
    result_convertion.remove(result_list[0])
    asserts.assertTrue(result_list[1] in result_convertion)
    result_convertion.remove(result_list[1])
    asserts.assertTrue(result_list[2] in result_convertion)
    result_convertion.remove(result_list[2])
    asserts.assertTrue(result_list[3] in result_convertion)
    result_convertion.remove(result_list[3])
    asserts.assertTrue(result_list[4] in result_convertion)
    result_convertion.remove(result_list[4])
    result_list = test_util.do_test_request("POST", path, 200, body={ "page_size": 5, "page": 2 })
    asserts.assertEqual(2, len(result_list))
    asserts.assertTrue(result_list[0] in result_convertion)
    result_convertion.remove(result_list[0])
    asserts.assertTrue(result_list[1] in result_convertion)

def test_search_field(asserts: TestCase, path, spec, prop_spec, prop_name):
    generator = _create_generator(spec["x-test-search-seed"])
    inserts = []
    for index in range(0, 8):
        inserts.append(generator())
    converter = _create_convert(spec)
    result_convertion = list(map(converter, inserts))

    required_response = None
    model_prop_name = prop_name if "x-query-target" not in prop_spec else prop_spec["x-query-target"]
    query_type = prop_spec["x-query-type"]
    body = { }
    if query_type == "in":
        body[prop_name] = []
        for insert in inserts:
            try:
                check = insert[model_prop_name][0] if isinstance(insert[model_prop_name], list) else insert[model_prop_name]
                body[prop_name].append(check)
                required_response = [converter(insert)]
                break
            except:
                pass
    if query_type == "eq":
        body[prop_name] = inserts[4][model_prop_name]
        required_response = [result_convertion[4]]
    if query_type == "lte":
        body[prop_name] = inserts[5][model_prop_name]
        required_response = [result_convertion[5]]
    if query_type == "gte":
        body[prop_name] = inserts[2][model_prop_name]
        required_response = [result_convertion[2]]

    result_list = test_util.do_test_request("POST", path, 200, body=body)
    asserts.assertTrue(len(required_response) <= len(result_list))
    for check in required_response:
        asserts.assertTrue(check in result_list)

# ===================================================================
#
# test setup
#
# ===================================================================

test_input = argparse.ArgumentParser()
test_input.add_argument("--path", type=str, default="")
input_args = test_input.parse_args()

collector = test_runner.TestSuiteCollector()

@collector.setup
def clean_databases(*args, **kwargs):
    test_util.mongo_clear_database()

def create_tests_for_spec(method, path, spec):
    # base on method verb
    if method == 'get':
        collector.test_with_data(test_simple_get_request, { "path": path, "spec": spec })
    if method == 'delete':
        collector.test_with_data(test_simple_delete_request, { "path": path, "spec": spec })
    if method == 'put':
        collector.test_with_data(test_simple_put_request, { "path": path, "spec": spec })
    if method == 'post':
        collector.test_with_data(test_simple_post_request, { "path": path, "spec": spec })

    if method == 'post' or method == 'put':
        collector.test_with_data(test_empty_body, { "method": method, "path": path, "spec": spec })
        input_model = _get_body_param_schema(spec)
        for key in input_model["properties"]:
            collector.test_with_data(test_missing_field_in_body,
                                     { "method": method, "path": path, "spec": spec, "missing": key })

    # extra ones from swagger
    if "x-test-create-endpoint" in spec:
        collector.test_with_data(test_double_insert_fails, { "method": method, "path": path, "spec": spec })
    if "x-test-search-seed" in spec:
        search_model = _get_body_param_schema(spec)
        collector.test_with_data(test_search_paging, { "path": path, "spec": spec })
        for key in search_model["properties"]:
            search_prop = search_model["properties"][key]
            if "x-query-type" not in search_prop:
                continue
            collector.test_with_data(test_search_field,
                                     { "path": path, "spec": spec, "prop_spec": search_prop, "prop_name": key })
    if "x-test-get-by-id" in spec:
        collector.test_with_data(test_simple_get_by_id, { "path": path, "spec": spec })
    if "x-test-delete-by-id" in spec:
        collector.test_with_data(test_simple_delete_by_id, { "path": path, "spec": spec })

def build_tests():
    for path_check in model.swagger["paths"]:
        if input_args.path != "" and input_args.path != path_check:
            continue
        path_spec = model.swagger["paths"][path_check]
        for verb in path_spec:
            create_tests_for_spec(verb, path_check, path_spec[verb])

build_tests()
collector.run()
