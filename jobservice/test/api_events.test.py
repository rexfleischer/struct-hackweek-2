from unittest import TestCase
import commons_test.test_runner as test_runner
import commons_test.test_util as test_util
import commons.mongo as mongo

collector = test_runner.TestSuiteCollector()

@collector.setup
def test_setup(asserts: TestCase):
    mongo.get_events_dao()._collection.delete_many({ })

@collector.test
def test_event_not_found(asserts: TestCase):
    response = test_util.do_test_request("GET", "/events/123", 404)
    asserts.assertEqual(response, { "message": "event not found", "status": 404 })

@collector.test
def test_event_find_by_id(asserts: TestCase):
    event = test_util.seed_random_events(1)[0]
    response = test_util.do_test_request("GET", "/events/{}".format(event["_id"]), 200)
    asserts.assertEqual(response, event)

@collector.test
def test_event_search_illegal_1(asserts: TestCase):
    test_util.do_test_request("POST", "/events", 400, body={ "page_size": "hello" })

@collector.test
def test_event_search_illegal_2(asserts: TestCase):
    test_util.do_test_request("POST", "/events", 400, body={ "key-not-there": "hello" })

@collector.test
def test_event_search_empty(asserts: TestCase):
    events = list(map(test_util.transform_event_to_summary, test_util.seed_random_events(5)))
    events_check = test_util.do_test_request("POST", "/events", 200, body={ })
    asserts.assertEqual(5, len(events_check))
    asserts.assertTrue(events[0] in events_check)
    asserts.assertTrue(events[1] in events_check)
    asserts.assertTrue(events[2] in events_check)
    asserts.assertTrue(events[3] in events_check)
    asserts.assertTrue(events[4] in events_check)

@collector.test
def test_event_search_page_1(asserts: TestCase):
    events = list(map(test_util.transform_event_to_summary, test_util.seed_random_events(12)))
    events_check = test_util.do_test_request("POST", "/events", 200, body={ "page_size": 5, "page": 0 })
    asserts.assertEqual(5, len(events_check))
    asserts.assertTrue(events[0] in events_check)
    asserts.assertTrue(events[1] in events_check)
    asserts.assertTrue(events[2] in events_check)
    asserts.assertTrue(events[3] in events_check)
    asserts.assertTrue(events[4] in events_check)
    events_check = test_util.do_test_request("POST", "/events", 200, body={ "page_size": 5, "page": 1 })
    asserts.assertEqual(5, len(events_check))
    asserts.assertTrue(events[5] in events_check)
    asserts.assertTrue(events[6] in events_check)
    asserts.assertTrue(events[7] in events_check)
    asserts.assertTrue(events[8] in events_check)
    asserts.assertTrue(events[9] in events_check)
    events_check = test_util.do_test_request("POST", "/events", 200, body={ "page_size": 5, "page": 2 })
    asserts.assertEqual(2, len(events_check))
    asserts.assertTrue(events[10] in events_check)
    asserts.assertTrue(events[11] in events_check)

@collector.test
def test_event_search_tags(asserts: TestCase):
    events = list(map(test_util.transform_event_to_summary, test_util.seed_random_events(10)))
    events_check = test_util.do_test_request("POST", "/events", 200, body={ "tags": ["stuff1:1"] })
    asserts.assertEqual(5, len(events_check))
    asserts.assertTrue(events[1] in events_check)
    asserts.assertTrue(events[3] in events_check)
    asserts.assertTrue(events[5] in events_check)
    asserts.assertTrue(events[7] in events_check)
    asserts.assertTrue(events[9] in events_check)

collector.run()
