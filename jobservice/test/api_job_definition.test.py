from unittest import TestCase
import pprint
import commons_test.test_runner as test_runner
import commons_test.test_util as test_util

collector = test_runner.TestSuiteCollector()

@collector.setup
def test_setup(asserts: TestCase):
    test_util.mongo_clear_database()

@collector.test
def test_create_entire_job(asserts: TestCase):
    job_upload_response = test_util.do_test_request("POST", "/job/definition/upload", 200, body={
        "project": "unit-testing",
        "name": "test_create_entire_job",
        "environment": { "SOME_ENV": "123" },
        "tags": ["yaya"],
        "stages": [
            {
                "name": "job_stage_1",
                "image": "127.0.0.1:30080/job-images/some-stage",
                "image_version": "latest",
                "image_entry_override": "",
                "image_command_override": "",
                "retry_max": 3,
                "environment": { "SOME_JOB_1_ENV": "234" },
                "environment_input_required": [],
                "tags": ["job_stage_1-awesome"],
                "cache_path_in": "",
                "cache_path_job": "",
                "cache_path_out": ""
            }
        ]
    })

    pprint.pprint(job_upload_response)

    job_run_response = test_util.do_test_request("POST", "/job/run", 200, body={
        "job_definition_id": job_upload_response["job_id"],
        "input_environment": { },
        "input_tags": ["local-manual"]
    })

    pprint.pprint(job_run_response)

collector.run()
