import unittest

class TestInjector(unittest.TestCase):
    def __init__(self, test_function, test_setup, test_teardown, test_input):
        super().__init__()
        self._test_function = test_function
        self._test_setup = test_setup
        self._test_teardown = test_teardown
        self._test_input = test_input

    def setUp(self):
        if self._test_setup is not None:
            try:
                self._test_setup(self, **self._test_input)
            except:
                print(">>>>>", "=" * 60)
                print(str(self._test_function))
                print("<<<<<", "=" * 60)
                raise

    def tearDown(self):
        if self._test_teardown is not None:
            try:
                self._test_teardown(self, **self._test_input)
            except:
                print(">>>>>", "=" * 60)
                print(str(self._test_function))
                print("<<<<<", "=" * 60)
                raise

    def runTest(self):
        try:
            self._test_function(self, **self._test_input)
        except:
            import pprint
            print(">>>>>", "=" * 60)
            print(str(self._test_function))
            print("--- input:")
            pprint.pprint(self._test_input)
            print("<<<<<", "=" * 60)
            raise

class TestSuiteCollector(object):
    def __init__(self):
        self._functions = []
        self._generators = []
        self._datas = []
        self._setup = None
        self._teardown = None
        self._test_with_data = []

    def data(self, data):
        self._datas.append(data)

    def test_with_data(self, func, data):
        self._test_with_data.append((func, data))

    def test(self, func):
        self._functions.append(func)
        return func

    def generator(self, func):
        self._generators.append(func)
        return func

    def setup(self, func):
        self._setup = func
        return func

    def teardown(self, func):
        self._teardown = func
        return func

    def run(self):
        suite = unittest.TestSuite()
        if len(self._datas) == 0 and len(self._generators) == 0:
            for function in self._functions:
                suite.addTest(TestInjector(function, self._setup, self._teardown, {}))
        else:
            for data in self._datas:
                for function in self._functions:
                    suite.addTest(TestInjector(function, self._setup, self._teardown, data))
            for generator in self._generators:
                for data in generator:
                    for function in self._functions:
                        suite.addTest(TestInjector(function, self._setup, self._teardown, data))

        for test_and_data in self._test_with_data:
            suite.addTest(TestInjector(test_and_data[0], self._setup, self._teardown, test_and_data[1]))

        runner = unittest.TextTestRunner()
        runner.run(suite)
