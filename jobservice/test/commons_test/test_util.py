import requests
import os
import json
import random
import string
import time
import uuid

import commons.mongo as mongo
import commons.model as model

def mongo_clear_database():
    mongo.get_events_dao()._collection.delete_many({})
    mongo.get_job_definition_dao()._collection.delete_many({})
    mongo.get_job_run_attempt_dao()._collection.delete_many({})
    mongo.get_job_run_dao()._collection.delete_many({})

# ===================================================================
#
# generate model
#
# ===================================================================

def generate_event(seed):
    tag1_stuff = "stuff1:" + str(seed % 2)
    tag2_stuff = "stuff2:" + str(seed % 3)
    result = model.event_create("project-" + str(seed % 3), "job_def_created", "test message " + str(seed), [tag1_stuff, tag2_stuff], { "blah": True })
    result["event_time"] = int(time.time()) + seed
    return result

def generate_job_definition(seed):
    return model.job_definition_create("project-" + str(seed % 3), "name - " + str(seed))

def generate_job_run(seed):
    tag1_stuff = "stuff1:" + str(seed % 2)
    tag2_stuff = "stuff2:" + str(seed % 3)
    return model.job_run_create(
        job_definition=generate_job_definition(seed),
        input_environment={
            "stuff": seed,
            "ok": seed % 1 == 0
        },
        tags=[tag1_stuff, tag2_stuff]
    )

def generate_job_run_stage(seed):
    return model.job_run_attempt_create(str(uuid.uuid4()), "job" + str(seed), seed % 4)

# ===================================================================
#
# generate random from schema
#
# ===================================================================

def _generate_random_boolean(prop_spec):
    return random.random() < 0.5

def _generate_random_integer(prop_spec):
    min = -100_000 if "minimum" not in prop_spec else prop_spec["minimum"]
    max = 100_000 if "maximum" not in prop_spec else prop_spec["maximum"]
    return random.randint(min, max)

def _generate_random_number(prop_spec):
    return random.uniform(-100_000, 100_000)

def _generate_random_string(prop_spec):
    if "enum" in prop_spec:
        return random.choice(prop_spec["enum"])
    string_format = prop_spec.get("format", None)
    if string_format == "uuid":
        return str(uuid.uuid4())
    size = random.randint(3, 15)
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=size))

def _generate_random_array(prop_spec):
    count = random.randint(-1, 4)
    result = []
    if count <= 0:
        return result
    sub_spec = prop_spec["items"]
    for index in range(0, count):
        result.append(generate_random_from_spec(sub_spec))
    return result

def _generate_random_object(prop_spec):
    result = { }
    if prop_spec.get("additionalProperties", False) and "properties" not in prop_spec:
        count = random.randint(-1, 4)
        if count > 0:
            for index in range(0, count):
                key = _generate_random_string({ })
                result[key] = generate_random_value()
    else:
        for key in prop_spec['properties']:
            result[key] = generate_random_from_spec(prop_spec['properties'][key])
    return result

def generate_random_value():
    datatype = ["boolean", "integer", "number", "string"]
    return generate_random_from_spec({ "type": random.choice(datatype) })

def generate_random_from_spec(prop_spec):
    if "$ref" in prop_spec:
        return generate_random_type(prop_spec["$ref"].split("/")[-1])
    prop_type = prop_spec["type"]
    if prop_type == "boolean":
        return _generate_random_boolean(prop_spec)
    if prop_type == "integer":
        return _generate_random_integer(prop_spec)
    if prop_type == "number":
        return _generate_random_integer(prop_spec)
    if prop_type == "string":
        return _generate_random_string(prop_spec)
    if prop_type == "array":
        return _generate_random_array(prop_spec)
    if prop_type == "object":
        return _generate_random_object(prop_spec)

def generate_random_type(type_name):
    return generate_random_from_spec(model.model_schemas[type_name])

# ===================================================================
#
# transforms
#
# ===================================================================

def transform_event_to_summary(event):
    return {
        "_id": event["_id"],
        "project": event["project"],
        "event_time": event["event_time"],
        "event_type": event["event_type"],
        "message": event["message"]
    }

# ===================================================================
#
# seed
#
# ===================================================================

def seed_random_events(count=40):
    events_dao = mongo.get_events_dao()
    result = []
    for index in range(0, count):
        generated = generate_event(index)
        events_dao.insert(generated)
        result.append(generated)
    return result

# ===================================================================
#
# http
#
# ===================================================================

test_endpoint = os.environ["HTTP_TEST_ENDPOINT"]

def do_test_request(method, path, expected_code_or_codes, body=None):
    actual_body = None
    headers = {
        "Accept": "application/json"
    }
    if body is not None:
        actual_body = json.dumps(body)
        headers["Content-Type"] = "application/json"
    response = requests.request(method, test_endpoint + path, data=actual_body, headers=headers, timeout=5)
    request = response.request
    is_code_expected = response.status_code == expected_code_or_codes
    if not is_code_expected and isinstance(expected_code_or_codes, list):
        is_code_expected = response.status_code in expected_code_or_codes
    if not is_code_expected:
        print("====================", response.request.method)
        print(">  url:", request.url)
        print(">  headers:")
        for header in request.headers:
            print(">   -", header, "=", request.headers[header])
        if actual_body is not None:
            print(">  body:")
            print(actual_body)
        # print out the response
        print("<  response:", response.status_code)
        print("<  headers:")
        for header in response.headers:
            print(">   -", header, "=", response.headers[header])
        print("<  body:")
        print(response.text)
        raise Exception("unexpected status code for " + path)
    return response.json()
