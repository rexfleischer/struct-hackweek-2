import commons_test.test_runner as test_runner
import commons_test.test_util as test_util
import commons.model as model
from copy import deepcopy

collector = test_runner.TestSuiteCollector()

for name in model.model_schemas:
    collector.data({ "datatype": name })

@collector.test
def test_generate_data(asserts, datatype):
    generated = test_util.generate_random_type(datatype)
    datatype_schema = deepcopy(model.model_schemas[datatype])
    datatype_schema["definitions"] = model.model_schemas
    model.validate(generated, datatype_schema)

if __name__ == '__main__':
    collector.run()
