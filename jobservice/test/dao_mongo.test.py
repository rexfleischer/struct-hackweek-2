import commons_test.test_runner as test_runner
import commons_test.test_util as test_util
import commons.mongo as mongo

collector = test_runner.TestSuiteCollector()

# ===================================================================
#
# data setup
#
# ===================================================================

collector.data({ "dao": mongo.get_events_dao(), "generator": test_util.generate_event })
collector.data({ "dao": mongo.get_job_definition_dao(), "generator": test_util.generate_job_definition })
collector.data({ "dao": mongo.get_job_run_dao(), "generator": test_util.generate_job_run })
collector.data({ "dao": mongo.get_job_run_attempt_dao(), "generator": test_util.generate_job_run_stage })

# ===================================================================
#
# actual tests
#
# ===================================================================

@collector.setup
def test_setup(asserts, dao: mongo.MongoDao, generator):
    dao._collection.delete_many({ })

@collector.test
def test_find_not_found(asserts, dao: mongo.MongoDao, generator):
    check = dao.find_by_id("doesnt-exist")
    asserts.assertIsNone(check)

@collector.test
def test_find_by_id(asserts, dao: mongo.MongoDao, generator):
    insert = generator(0)
    dao.upsert(insert)
    check = dao.find_by_id(insert["_id"])
    asserts.assertEqual(insert, check)

@collector.test
def test_find_by_ids(asserts, dao: mongo.MongoDao, generator):
    inserts = [generator(0), generator(1), generator(2)]
    for insert in inserts:
        asserts.assertTrue(dao.upsert(insert))
    ids = [insert["_id"] for insert in inserts]
    checks = dao.find_by_ids(ids)

    asserts.assertEqual(3, len(checks))
    asserts.assertTrue(checks[0] in inserts)
    asserts.assertTrue(checks[1] in inserts)
    asserts.assertTrue(checks[2] in inserts)

@collector.test
def test_delete_by_id(asserts, dao: mongo.MongoDao, generator):
    insert = generator(0)
    dao.upsert(insert)
    asserts.assertEqual(insert, dao.find_by_id(insert["_id"]))
    dao.delete_by_id(insert["_id"])
    asserts.assertIsNone(dao.find_by_id(insert["_id"]))

# ===================================================================
#
# execution
#
# ===================================================================

if __name__ == '__main__':
    collector.run()
