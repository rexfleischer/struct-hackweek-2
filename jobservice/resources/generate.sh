#!/usr/bin/env bash

docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate \
    -i /local/openapi.datatypes.yaml \
    -g csharp \
    -o /local/out/csharp

docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate \
    -i /local/openapi.datatypes.yaml \
    -g kotlin \
    -o /local/out/kotlin

docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate \
    -i /local/openapi.datatypes.yaml \
    -g python \
    -o /local/out/python