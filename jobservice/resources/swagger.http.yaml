swagger: "2.0"
info:
  description: ""
  version: "1.0.0"
  title: "swagger for job service api"
basePath: "/v1"
schemes:
  - "http"
consumes:
  - "application/json"
produces:
  - "application/json"
paths:

  # =================================================================
  # =================================================================
  # probe endpoints
  #
  /healthcheck:
    get:
      operationId: "api_health.is_healthy"
      responses:
        200:
          description: list of event summaries
          schema:
            $ref: '#/definitions/OkResponse'
        500:
          description: event not found
          schema:
            $ref: '#/definitions/ErrorResponse'

  # =================================================================
  # =================================================================
  # event endpoints
  #
  /events:
    post:
      operationId: "api_events.events_search"
      x-test-search-seed: Event
      parameters:
        - in: body
          name: body
          description: the _id of the pipeline run. this includes job execs
          required: true
          schema:
            $ref: '#/definitions/EventSearchRequest'
      responses:
        200:
          description: list of event summaries
          schema:
            type: array
            items:
              $ref: '#/definitions/EventSummary'
        400:
          description: input not valid
          schema:
            $ref: '#/definitions/ErrorResponse'
  /events/{event_id}:
    get:
      summary: "gets a single event"
      description: ""
      operationId: "api_events.events_get_by_id"
      x-test-get-by-id: true
      parameters:
        - in: path
          name: event_id
          description: the _id of the event
          required: true
          type: string
      responses:
        200:
          description: the details of a single event
          schema:
            $ref: '#/definitions/Event'
        404:
          description: event not found
          schema:
            $ref: '#/definitions/ErrorResponse'

  # =================================================================
  # =================================================================
  # job definition endpoints
  #
  /job/definition:
    post:
      summary: "create a new job definition with no stages or data"
      description: ""
      operationId: "api_job_definition.create"
      x-test-create-endpoint: true
      parameters:
        - in: body
          name: "body"
          description: ""
          required: true
          schema:
            $ref: "#/definitions/JobDefinitionCreateRequest"
      responses:
        200:
          description: the new job definition
          schema:
            $ref: '#/definitions/JobDefinition'
        400:
          description: unable to insert JobDefinition
          schema:
            $ref: '#/definitions/ErrorResponse'
    put:
      summary: update a job definition
      operationId: "api_job_definition.update"
      parameters:
        - in: body
          name: "body"
          description: "the complete job to update"
          required: true
          schema:
            $ref: "#/definitions/JobDefinition"
      responses:
        200:
          description: JobDefinition updated
          schema:
            $ref: '#/definitions/OkResponse'
        400:
          description: unable to update JobDefinition
          schema:
            $ref: '#/definitions/ErrorResponse'
        404:
          description: JobDefinition not found
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/definition/upload:
    post:
      summary: "create a new job definition with no stages or data"
      description: ""
      operationId: "api_job_definition.upload"
      parameters:
        - in: body
          name: "body"
          description: ""
          required: true
          schema:
            $ref: "#/definitions/JobDefinitionUploadRequest"
      responses:
        200:
          description: the new job definition
          schema:
            $ref: '#/definitions/JobDefinitionUploadResponse'
        400:
          description: unable to insert JobDefinition
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/definition/search:
    post:
      summary: "search job definitions"
      description: ""
      operationId: "api_job_definition.search"
      x-test-search-seed: JobDefinition
      parameters:
        - in: body
          name: "body"
          description: "Pet object that needs to be added to the store"
          required: true
          schema:
            $ref: "#/definitions/JobDefinitionSearchRequest"
      responses:
        200:
          description: successful
          schema:
            type: array
            items:
              $ref: '#/definitions/JobDefinitionSummary'
  /job/definition/{job_definition_id}:
    get:
      summary: "get a job definition by id"
      description: ""
      operationId: "api_job_definition.get_by_id"
      x-test-get-by-id: true
      parameters:
        - in: path
          name: job_definition_id
          description: the _id of the job definition
          required: true
          type: string
      responses:
        200:
          description: the new job definition
          schema:
            $ref: '#/definitions/JobDefinition'
        404:
          description: job definition not found
          schema:
            $ref: '#/definitions/ErrorResponse'
    delete:
      summary: "delete a job definition"
      description: ""
      operationId: "api_job_definition.delete_by_id"
      x-test-delete-by-id: JobDefinition
      parameters:
        - in: path
          name: job_definition_id
          description: the _id of the job definition
          required: true
          type: string
      responses:
        200:
          description: job definition deleted
          schema:
            $ref: '#/definitions/DeleteResponse'

  # =================================================================
  # =================================================================
  # job run endpoints
  #
  /job/run:
    post:
      summary: "create a job run"
      description: "request the starting a job"
      operationId: "api_job_run.start"
      parameters:
        - in: "body"
          name: "body"
          required: true
          schema:
            $ref: "#/definitions/JobRunStartRequest"
      responses:
        200:
          description: "job has been queued"
          schema:
            $ref: '#/definitions/JobRunSummary'
        400:
          description: job run unable to start
          schema:
            $ref: '#/definitions/ErrorResponse'
        404:
          description: job run not found
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/run/search:
    post:
      summary: "create a job run"
      description: "request the starting a job"
      operationId: "api_job_run.search"
      x-test-search-seed: JobRun
      parameters:
        - in: "body"
          name: "body"
          required: true
          schema:
            $ref: "#/definitions/JobRunSearchRequest"
      responses:
        200:
          description: "started"
          schema:
            type: array
            items:
              $ref: '#/definitions/JobRunSummary'
        400:
          description: cannot search for job runs
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/run/{job_run_id}:
    get:
      summary: "get the details of a job run"
      description: ""
      operationId: "api_job_run.get_by_id"
      parameters:
        - in: path
          name: job_run_id
          required: true
          type: string
      responses:
        200:
          description: "started"
          schema:
            $ref: '#/definitions/JobRun'
        404:
          description: job run not found
          schema:
            $ref: '#/definitions/ErrorResponse'
    delete:
      summary: "cancels the job by id"
      description: ""
      operationId: "api_job_run.cancel_by_id"
      parameters:
        - in: path
          name: job_run_id
          required: true
          type: string
      responses:
        200:
          description: "started"
          schema:
            $ref: '#/definitions/OkResponse'
        404:
          description: job run not found
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/run/{job_run_id}/attempts:
    get:
      summary: "get a the logs for the give job stage"
      description: ""
      operationId: "api_job_run.get_attempt_summaries_by_id"
      parameters:
        - in: path
          name: job_run_id
          required: true
          type: string
      responses:
        200:
          description: "logs"
          schema:
            type: array
            items:
              $ref: '#/definitions/JobRunAttemptSummary'
        404:
          description: JobRun or JobStageRun not foun
          schema:
            $ref: '#/definitions/ErrorResponse'
  /job/run/{job_run_id}/attempts/{job_attempt_id}:
    get:
      summary: "get a the logs for the give job stage"
      description: ""
      operationId: "api_job_run.get_attempt_by_id"
      parameters:
        - in: path
          name: job_run_id
          required: true
          type: string
        - in: path
          name: job_attempt_id
          required: true
          type: string
      responses:
        200:
          description: "get full attempt"
          schema:
            $ref: '#/definitions/JobRunAttempt'
        404:
          description: JobRun or JobStageRun not foun
          schema:
            $ref: '#/definitions/ErrorResponse'

definitions:

  # =================================================================
  # =================================================================
  # general datatypes
  #
  ErrorResponse:
    type: object
    properties:
      status:
        type: integer
      message:
        type: string
  OkResponse:
    type: object
    required:
      - ok
    properties:
      ok:
        type: boolean
  DeleteResponse:
    type: object
    required:
      - deleted
    properties:
      deleted:
        type: boolean

  # =================================================================
  # =================================================================
  # event datatypes
  #
  EventType:
    type: string
    enum:
      - job_def_created
      - job_def_updated
      - job_def_deleted
      - job_run_start
      - job_run_update
      - job_run_end
      - job_run_stage_start
      - job_run_stage_update
      - job_run_stage_end
  EventSearchRequest:
    type: object
    additionalProperties: false
    properties:
      project:
        type: string
        x-query-type: eq
      time_min:
        type: integer
        x-query-type: gte
        x-query-target: event_time
      time_max:
        type: integer
        x-query-type: lte
        x-query-target: event_time
      types:
        type: array
        x-query-type: in
        x-query-target: event_type
        items:
          $ref: '#/definitions/EventType'
      tags:
        type: array
        x-query-type: in
        x-query-target: tags
        items:
          type: string
      page:
        type: integer
        default: 0
        minimum: 0
      page_size:
        type: integer
        default: 20
        minimum: 1
        maximum: 100
      sort_by:
        type: string
        default: event_time
      sort_direction:
        type: string
        default: DESC
        enum:
          - DESC
          - ASC
  EventSummary:
    type: object
    additionalProperties: false
    required:
      - _id
      - project
      - event_time
      - event_type
      - message
    properties:
      _id:
        type: string
        format: uuid
      project:
        type: string
      event_time:
        type: integer
      event_type:
        $ref: '#/definitions/EventType'
      message:
        type: string
  Event:
    type: object
    additionalProperties: false
    required:
      - _id
      - project
      - event_time
      - event_type
      - tags
      - message
      - details
    properties:
      _id:
        type: string
        format: uuid
      project:
        type: string
      event_time:
        type: integer
      event_type:
        $ref: '#/definitions/EventType'
      tags:
        type: array
        items:
          type: string
      message:
        type: string
      details:
        type: object
        additionalProperties: true

  # =================================================================
  # =================================================================
  # job definition datatypes
  #
  JobDefinitionSearchRequest:
    type: object
    additionalProperties: false
    properties:
      project:
        type: string
        x-query-type: eq
      names:
        type: array
        x-query-type: in
        x-query-target: name
        items:
          type: string
      tags:
        type: array
        x-query-type: in
        items:
          type: string
      page:
        type: integer
        default: 0
        minimum: 0
      page_size:
        type: integer
        default: 20
        minimum: 1
        maximum: 100
      sort_by:
        type: string
        default: event_time
      sort_direction:
        type: string
        default: DESC
        enum:
          - DESC
          - ASC
  JobDefinitionCreateRequest:
    type: object
    additionalProperties: false
    required:
      - project
      - name
    properties:
      project:
        type: string
      name:
        type: string
  JobDefinitionUploadRequest:
    type: object
    additionalProperties: false
    required:
      - project
      - name
      - environment
      - tags
    properties:
      project:
        type: string
      name:
        type: string
      environment:
        type: object
        additionalProperties: true
      tags:
        type: array
        items:
          type: string
      stages:
        type: array
        items:
          $ref: '#/definitions/JobStage'
  JobDefinitionUploadResponse:
    type: object
    required:
      - job_id
      - was_insert
    properties:
      job_id:
        type: string
        format: uuid
      was_insert:
        type: boolean
  JobDefinitionSummary:
    type: object
    additionalProperties: false
    required:
      - _id
      - project
      - name
    properties:
      _id:
        type: string
        format: uuid
      project:
        type: string
      name:
        type: string
  JobStage:
    type: object
    additionalProperties: false
    required:
      - name
      - image
      - image_version
      - image_entry_override
      - image_command_override
      - retry_max
      - environment
      - environment_input_required
      - tags
      - cache_path_in
      - cache_path_job
      - cache_path_out
    properties:
      name:
        type: string
      image:
        type: string
      image_version:
        type: string
      image_entry_override:
        type: string
      image_command_override:
        type: string
      retry_max:
        type: integer
        default: 3
      environment:
        type: object
        additionalProperties: true
      environment_input_required:
        type: array
        items:
          type: string
      tags:
        type: array
        items:
          type: string
      cache_path_in:
        type: string
      cache_path_job:
        type: string
      cache_path_out:
        type: string
  JobDefinition:
    type: object
    additionalProperties: false
    required:
      - _id
      - project
      - name
      - environment
      - tags
    properties:
      _id:
        type: string
        format: uuid
      project:
        type: string
      name:
        type: string
      environment:
        type: object
        additionalProperties: true
      tags:
        type: array
        items:
          type: string
      stages:
        type: array
        items:
          $ref: '#/definitions/JobStage'

  # =================================================================
  # =================================================================
  # job run datatypes
  #
  JobRunSearchRequest:
    type: object
    additionalProperties: false
    properties:
      start_min:
        type: integer
        x-query-type: gte
        x-query-target: start
      start_max:
        type: integer
        x-query-type: lte
        x-query-target: start
      finished_min:
        type: integer
        x-query-type: gte
        x-query-target: finished
      finished_max:
        type: integer
        x-query-type: lte
        x-query-target: finished
      states:
        type: array
        x-query-type: in
        x-query-target: state
        items:
          $ref: '#/definitions/JobRunState'
      tags:
        type: array
        x-query-type: in
        items:
          type: string
      job_definition_ids:
        type: array
        x-query-type: in
        x-query-target: job_definition_id
        items:
          type: string
      page:
        type: integer
        default: 0
        minimum: 0
      page_size:
        type: integer
        default: 20
        minimum: 1
        maximum: 100
      sort_by:
        type: string
        default: event_time
      sort_direction:
        type: string
        default: DESC
        enum:
          - DESC
          - ASC
  JobRunStartRequest:
    type: object
    additionalProperties: false
    required:
      - job_definition_id
      - input_environment
      - input_tags
    properties:
      job_definition_id:
        type: string
      input_environment:
        type: object
        additionalProperties: true
      input_tags:
        type: array
        items:
          type: string
  JobRunState:
    type: string
    enum:
      - queue
      - start
      - execute
      - clean
      - finished_error
      - finished_success
      - finished_cancel
  JobRunAttemptSummary:
    type: object
    additionalProperties: false
    required:
      - _id
      - start
      - finished
      - state
      - job_run_id
      - job_pod_id
      - stage_index
    properties:
      _id:
        type: string
        format: uuid
      start:
        type: integer
      finished:
        type: integer
      state:
        $ref: '#/definitions/JobRunState'
      job_run_id:
        type: string
        format: uuid
      job_pod_id:
        type: string
      stage_index:
        type: integer
  JobRunAttempt:
    type: object
    additionalProperties: false
    required:
      - _id
      - start
      - finished
      - state
      - job_run_id
      - job_pod_id
      - stage_index
      - logs
    properties:
      _id:
        type: string
        format: uuid
      start:
        type: integer
      finished:
        type: integer
      state:
        $ref: '#/definitions/JobRunState'
      job_run_id:
        type: string
        format: uuid
      job_pod_id:
        type: string
      stage_index:
        type: integer
      logs:
        type: string
  JobRunStage:
    type: object
    additionalProperties: false
    required:
      - start
      - finished
      - state
    properties:
      start:
        type: integer
      finished:
        type: integer
      state:
        $ref: '#/definitions/JobRunState'
  JobRunSummary:
    type: object
    additionalProperties: false
    required:
      - _id
      - start
      - finished
      - state
      - job_name
      - job_definition_id
      - run_stages
      - tags
    properties:
      _id:
        type: string
        format: uuid
      start:
        type: integer
      finished:
        type: integer
      state:
        $ref: '#/definitions/JobRunState'
      job_name:
        type: string
      job_definition_id:
        type: string
        format: uuid
      run_stages:
        type: array
        items:
          $ref: '#/definitions/JobRunStage'
      tags:
        type: array
        items:
          type: string
  JobRun:
    type: object
    additionalProperties: false
    required:
      - _id
      - start
      - finished
      - state
      - job_name
      - job_definition_id
      - job_definition
      - run_stages
      - input_environment
      - tags
    properties:
      _id:
        type: string
        format: uuid
      start:
        type: integer
      finished:
        type: integer
      state:
        $ref: '#/definitions/JobRunState'
      job_name:
        type: string
      job_definition_id:
        type: string
        format: uuid
      job_definition:
        $ref: '#/definitions/JobDefinition'
      run_stages:
        type: array
        items:
          $ref: '#/definitions/JobRunStage'
      input_environment:
        type: object
        additionalProperties: true
      tags:
        type: array
        items:
          type: string
