import argparse
from kubernetes import client, config
import subprocess
import os
import time
import os.path

args_parser = argparse.ArgumentParser()
args_parser.add_argument("role", choices=["init", "post"])
args = args_parser.parse_args()
sidecar_role = args.role
pod_name = os.environ["HOSTNAME"]
object_name_in = os.environ.get("S3_OBJECT_NAME_IN", None)
object_name_job = os.environ.get("S3_OBJECT_NAME_JOB", None)
object_name_out = os.environ.get("S3_OBJECT_NAME_OUT", None)
state = {}

def ensure_s3_host_added():
    if "s3-added" not in state:
        cli_command("./mc config host add job-cache http://minio.job-service.svc.cluster.local:9000/ hello-accessKey hello-secretKey")
    state["s3-added"] = True

def get_env_or_raise(name):
    try:
        return os.environ[name]
    except:
        print("{} is a require env".format(name))
        raise

def cli_command(cmd):
    print(">>", cmd)
    try:
        print(subprocess.check_output(cmd, shell=True).decode())
    except subprocess.CalledProcessError as ex:
        print(">> exited with code: ", str(ex.returncode))
        print("============ stdout")
        print(ex.stdout)
        print("============ stderr")
        print(ex.stderr)
        raise

def print_ls_state():
    cli_command("ls -la")
    if os.path.isdir("./in_volume"):
        cli_command("ls -la in_volume")
    if os.path.isdir("./job_volume"):
        cli_command("ls -la job_volume")
    if os.path.isdir("./out_volume"):
        cli_command("ls -la out_volume")

def pull_s3_object(object_name, volume_name, failure_ok):
    if object_name is None or not object_name:
        raise Exception("object name not found")
    ensure_s3_host_added()
    print("pulling object job-cache/job-cache-bucket/{} to {}".format(object_name, volume_name))
    try:
        cli_command("./mc cp job-cache/job-cache-bucket/{} ./{}.tar.gz".format(object_name, volume_name))
    except:
        if failure_ok:
            return
        raise
    cli_command("tar -xf ./{}.tar.gz -C {}".format(volume_name, volume_name))
    print("finished pulling object")

def push_s3_object(object_name, volume_name):
    if object_name is None or not object_name:
        raise Exception("object name not found")
    ensure_s3_host_added()
    print("pushing object to job-cache/job-cache-bucket/{} from {}".format(object_name, volume_name))
    cli_command("tar -zcvf {}.tar.gz -C /data/{} .".format(volume_name, volume_name))
    cli_command("./mc cp ./{}.tar.gz job-cache/job-cache-bucket/{}".format(volume_name, object_name))
    print("finished pushing object")

def is_job_container_finished(check: client.V1Pod):
    check_status: client.V1PodStatus = check.status
    for status in check_status.container_statuses:
        if status.name != "job":
            continue
        terminated: client.V1ContainerStateTerminated = status.state.terminated
        if terminated is None:
            return None
        return terminated.exit_code
    # should not get here. this means the job container was not found and
    # we either dont have the containers setup correctly or this container
    # is configured incorrectly
    raise Exception("unable to find job container")

def wait_for_job_and_save_logs():
    import commons.mongo as mongo
    import commons.model as model

    job_namespace = get_env_or_raise("K8S_JOB_EXEC_NAMESPACE")
    job_run_id = get_env_or_raise("JOB_RUN_ID")
    job_run_stage_index = int(get_env_or_raise("JOB_RUN_STAGE_INDEX"))

    config.load_incluster_config()
    core_api = client.CoreV1Api()
    print("waiting for job container to terminate")
    while True:
        check: client.V1Pod = core_api.read_namespaced_pod(pod_name, job_namespace)
        result = is_job_container_finished(check)
        if result is not None:
            status_code = result
            break
        print(".", end='')
        time.sleep(1)
    print()
    print("finished waiting for container to terminated")
    print("saving job attempt to logs")
    job_run_attempt = model.job_run_attempt_create(job_run_id, pod_name, job_run_stage_index)
    job_run_attempt["logs"] = core_api.read_namespaced_pod_log(pod_name, job_namespace, container="job")
    mongo.get_job_run_attempt_dao().insert(job_run_attempt)
    print("finished saving job attempt")
    return status_code

def main():
    if sidecar_role == "init":
        if object_name_in is not None:
            pull_s3_object(object_name_in, "in_volume", False)
        if object_name_job is not None:
            pull_s3_object(object_name_job, "job_volume", True)
    elif sidecar_role == "post":
        job_exit_code = wait_for_job_and_save_logs()
        if job_exit_code == 0:
            print("looks like the job was a success!")
            if object_name_out is not None:
                push_s3_object(object_name_out, "out_volume")
        else:
            print("looks like the job failed...")
            if object_name_job is not None:
                push_s3_object(object_name_job, "job_volume")

if __name__ == '__main__':
    main()
