import http_api.api_util as api_util
import logging
import commons.mongo as mongo
import commons.model as model

logger = logging.getLogger("api.events")

events_dao = mongo.get_events_dao()

def events_get_by_id(event_id):
    check = events_dao.find_by_id(event_id)
    logger.info("%s => %s", event_id, check)
    return api_util.return_200_or_404_if_none(check, "event not found")

def events_search(body):
    result = events_dao.search(
        query=api_util.search_query(body, model.model_schemas["EventSearchRequest"]),
        projection=api_util.search_projections(model.model_schemas["EventSummary"]),
        **api_util.search_standard_pluck(body)
    )
    return list(result)
