import requests
import os
import logging

logger = logging.getLogger()
orchestrator_url = os.environ["ORCHESTRATOR_URL"]

def send_start_job(job_run_id):
    endpoint = orchestrator_url + "/job/" + job_run_id
    logger.info("request POST %s", endpoint)
    response = requests.request("POST", orchestrator_url + "/job/" + job_run_id, timeout=5)
    logger.info("response from POST %s => %s", endpoint, response.text)
    response.raise_for_status()

def send_cancel_job(job_run_id):
    endpoint = orchestrator_url + "/job/" + job_run_id
    logger.info("request DELETE %s", endpoint)
    response = requests.request("DELETE", orchestrator_url + "/job/" + job_run_id, timeout=5)
    logger.info("response from DELETE %s => %s", endpoint, response.text)
    response.raise_for_status()
