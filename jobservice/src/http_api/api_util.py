
def return_200_or_404_if_none(check, message):
    if check is None:
        return { "status": 404, "message": message }, 404
    return check, 200

def search_standard_pluck(search):
    page = 0 if "page" not in search else search["page"]
    page_size = 20 if "page_size" not in search else search["page_size"]
    sort_by = "event_time" if "sort_by" not in search else search["sort_by"]
    sort_direction_request = "ASC" if "sort_direction" not in search else search["sort_direction"]
    sort_direction = 1 if sort_direction_request == "ASC" else -1
    return {
        "skip": page * page_size,
        "limit": page_size,
        "sort": [(sort_by, sort_direction)]
    }

def search_query(search, definition):
    query = []
    for key in definition["properties"]:
        if key not in search:
            continue
        prop_definition = definition["properties"][key]
        if "x-query-type" not in prop_definition:
            continue
        target = key
        if "x-query-target" in prop_definition:
            target = prop_definition["x-query-target"]
        query.append({ target: { "${}".format(prop_definition["x-query-type"]): search[key] } })
    if len(query) > 0:
        return { "$and": query }
    return { }

def search_projections(definition):
    result = { }
    for key in definition["properties"]:
        result[key] = 1
    return result
