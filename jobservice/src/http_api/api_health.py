import commons.mongo as mongo

def is_healthy():
    mongo.mongo_status_check()
    return { "ok": True }, 200
