import logging
import connexion
import os

logging.basicConfig(level=logging.INFO)

swagger_location = os.environ['SWAGGER_HTTP_LOCATION']
app = connexion.FlaskApp(__name__)
app.add_api(swagger_location, strict_validation=True, validate_responses=True)
application = app.app

if __name__ == '__main__':

    import commons.mongo

    # TODO: this should be done somewhere else
    commons.mongo.mongo_do_indexing()

    app.run(port=8080)
