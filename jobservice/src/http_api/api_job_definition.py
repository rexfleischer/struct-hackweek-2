import http_api.api_util as api_util
import commons.mongo as mongo
import commons.model as model
import logging
import uuid

logger = logging.getLogger("api.events")

job_definition_dao = mongo.get_job_definition_dao()

def create(body):
    new_job_definition = model.job_definition_create(body["project"], body["name"])
    insert_error = job_definition_dao.insert(new_job_definition)
    if insert_error is None:
        return new_job_definition, 200
    return { "status": 400, "message": insert_error }, 400

def upload(body):
    old_check = job_definition_dao.search_one(
            query={ "project": body["project"], "name": body["name"] },
            projection={ "_id": 1 }
    )
    body["_id"] = old_check["_id"] if old_check is not None else str(uuid.uuid4())
    was_insert = job_definition_dao.upsert(body)
    return { "job_id": body["_id"], "was_insert": was_insert }

def update(body):
    was_updated = job_definition_dao.replace(body)
    if was_updated:
        return { "ok": True }, 200
    return { "status": 404, "message": "JobDefinition not found" }, 404

def search(body):
    result = job_definition_dao.search(
            query=api_util.search_query(body, model.model_schemas["JobDefinitionSearchRequest"]),
            projection=api_util.search_projections(model.model_schemas["JobDefinitionSummary"]),
            **api_util.search_standard_pluck(body)
    )
    return result

def get_by_id(job_definition_id):
    check = job_definition_dao.find_by_id(job_definition_id)
    return api_util.return_200_or_404_if_none(check, "JobDefinition not found")

def delete_by_id(job_definition_id):
    was_deleted = job_definition_dao.delete_by_id(job_definition_id)
    return { "deleted": was_deleted }, 200
