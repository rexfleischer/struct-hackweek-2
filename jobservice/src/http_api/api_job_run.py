import http_api.api_util as api_util
import http_api.orchestrator as orchestrator
import commons.mongo as mongo
import commons.model as model

import logging
logger = logging.getLogger("api.events")

job_definition_dao = mongo.get_job_definition_dao()
job_run_dao = mongo.get_job_run_dao()
job_run_attempt_dao = mongo.get_job_run_attempt_dao()

def start(body):
    job_definition = job_definition_dao.find_by_id(body["job_definition_id"])
    if job_definition is None:
        return { "status": 404, "message": "JobDefinition not found" }, 404
    if len(job_definition["stages"]) == 0:
        return { "status": 400, "message": "JobDefinition has no stages" }, 400

    # make a list of all the required inputs from the jog stages
    # - then remove all the inputs that are put in from the JobDefinition
    # - then make sure the rest is there from the request
    inputs_required = []
    for stage in job_definition["stages"]:
        environment_input_required = stage["environment_input_required"]
        for check in environment_input_required:
            if check not in inputs_required:
                inputs_required.append(check)
    for key in job_definition["environment"]:
        try:
            inputs_required.remove(key)
        except ValueError:
            pass  # valid error
    for key in body["input_environment"]:
        try:
            inputs_required.remove(key)
        except ValueError:
            pass  # valid error
    if len(inputs_required) > 0:
        return { "status": 400, "message": "not all input keys send", "missing": inputs_required }, 400

    # gather all the tags
    job_tags = []
    for stage in job_definition["stages"]:
        for tag in stage["tags"]:
            if tag not in job_tags:
                job_tags.append(tag)
    for tag in job_definition["tags"]:
        if tag not in job_tags:
            job_tags.append(tag)
    for tag in body["input_tags"]:
        if tag not in job_tags:
            job_tags.append(tag)

    new_job_run = model.job_run_create(job_definition, body["input_environment"], job_tags)
    job_run_dao.insert(new_job_run)
    orchestrator.send_start_job(new_job_run["_id"])

    result = { }
    for key in model.model_schemas["JobRunSummary"]["properties"]:
        result[key] = new_job_run[key]
    return result

def search(body):
    result = job_run_dao.search(
        query=api_util.search_query(body, model.model_schemas["JobRunSearchRequest"]),
        projection=api_util.search_projections(model.model_schemas["JobRunSummary"]),
        **api_util.search_standard_pluck(body)
    )
    return result

def get_by_id(job_run_id):
    check = job_run_dao.find_by_id(job_run_id)
    return api_util.return_200_or_404_if_none(check, "JobRun not found")

def cancel_by_id(job_run_id):
    check = job_run_dao.find_by_id(job_run_id)
    if check is None:
        return { "statue": 404, "message": "JobRun not found" }, 404
    check["state"] = "finished_cancel"
    job_run_dao.replace(check)
    orchestrator.send_start_job(job_run_id)
    return { "ok": True }, 200

def get_attempt_summaries_by_id(job_run_id):
    return job_run_attempt_dao.search(
        query={ "job_run_id": job_run_id },
        projection=api_util.search_projections(model.model_schemas["JobRunAttemptSummary"]),
        sort=[("start", 1)]
    )

def get_attempt_by_id(job_run_id, job_attempt_id):
    check = job_run_attempt_dao.search_one(
        query={ "_id": job_attempt_id, "job_run_id": job_run_id }
    )
    return api_util.return_200_or_404_if_none(check, "JobRun not found")
