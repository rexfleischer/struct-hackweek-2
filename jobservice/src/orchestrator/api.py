import orchestrator.k8s_queues as k8s_queues
import commons.mongo as mongo

def is_healthy():
    mongo.mongo_status_check()
    return { "ok": True }, 200

def ensure_start(job_run_id):
    k8s_queues.queue_start_job.put(job_run_id)
    return { "ok": True }, 200

def ensure_stop(job_run_id):
    k8s_queues.queue_cancel_job.put(job_run_id)
    return { "ok": True }, 200
