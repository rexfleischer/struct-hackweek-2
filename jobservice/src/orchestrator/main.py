import logging
import connexion
import os

logging.basicConfig(level=logging.INFO)

swagger_location = os.environ['SWAGGER_ORCH_LOCATION']
app = connexion.FlaskApp(__name__)
app.add_api(swagger_location, strict_validation=True, validate_responses=True)
application = app.app

if os.environ['K8S_ENABLED'] == "true":
    import k8s

if __name__ == '__main__':
    app.run(port=8080)
