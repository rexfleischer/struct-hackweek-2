import threading
import _thread
import logging

logger = logging.getLogger()

def start_safe_thread(func):
    thread_name = func.__name__
    def thread_entry():
        logger.info("starting thread: " + thread_name)
        try:
            func()
        except BaseException as ex:
            logger.exception("unhandled exception in thread: " + thread_name)
            _thread.interrupt_main()
        logger.info("exiting thread: " + thread_name)

    thread = threading.Thread(target=thread_entry)
    thread.daemon = True
    thread.start()
    return thread_entry
