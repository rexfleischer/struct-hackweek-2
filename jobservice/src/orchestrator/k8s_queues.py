import queue

# we need to setup the queues like this because when we run tests
# in docker-compose, we cannot integrate with k8s, so we just fill
# the queue with no polling

queue_start_job = queue.Queue()
queue_job_finished_check = queue.Queue()
queue_cancel_job = queue.Queue()
