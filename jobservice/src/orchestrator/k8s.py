import orchestrator.thread_helper as thread_helper
import orchestrator.k8s_queues as k8s_queues
import commons.mongo as mongo
import commons.model as model

from kubernetes import client, config, watch
import logging
import jinja2
import os
import io
import yaml
import time

config.load_incluster_config()
logger = logging.getLogger()

job_namespace = os.environ["K8S_JOB_EXEC_NAMESPACE"]

dao_job_run = mongo.get_job_run_dao()
dao_job_run_attempt = mongo.get_job_run_attempt_dao()
dao_events = mongo.get_events_dao()

# ===================================================================
#
# listening to k8s job events
#
# ===================================================================

@thread_helper.start_safe_thread
def _listen_to_k8s_job_events_entry():
    batch_api = client.BatchV1Api()
    w = watch.Watch()
    for event in w.stream(batch_api.list_namespaced_job, namespace=job_namespace):
        event_type = event["type"]
        event_object = event.get("object", None)
        event_job_name = event_object.metadata.name
        event_labels = event_object.metadata.labels
        if event_labels is None or len(event_labels) == 0:
            logger.info("not handling message because pod has no labels")
            continue

        event_jobservice_label_mark = event_labels.get("fromjobservice", False)
        if not event_jobservice_label_mark:
            logger.info("not handling message because pod is not marked as from this job service")
            continue

        event_job_run_id = event_labels.get("job_run_id", None)
        job_stage_index = event_labels.get("job_stage_index", None)

        if event_job_run_id is None or not event_job_run_id or job_stage_index is None or not job_stage_index:
            logger.info("not handling message because pod has missing job labels: %s", event_labels)
            continue
        job_stage_index = int(job_stage_index)

        job_result_succeeded = event_object.status.succeeded == 1
        job_result_failed = event_object.status.failed == 1
        job_active = event_object.status.active == 1
        job_has_result = job_result_succeeded or job_result_failed
        result_event = {
            "event_type": event_type,
            "job_active": job_active,
            "job_name": event_job_name,
            "job_run_id": event_job_run_id,
            "job_stage_index": job_stage_index,
            "job_result_succeeded": job_result_succeeded,
            "job_result_failed": job_result_failed,
            "job_has_result": job_has_result
        }

        if result_event["job_active"]:
            logger.info("not handling message because job is still active: %s", result_event)
            continue

        if event_type == "ADDED":
            logger.info("received job added: %s", result_event)
        elif event_type == "DELETED":
            logger.info("received job deleted: %s", result_event)
        elif event_type == "MODIFIED":
            logger.info("received job modified: %s", result_event)
            k8s_queues.queue_job_finished_check.put(result_event)
        else:
            logger.warning("received unknown event type: %s", result_event)

# ===================================================================
#
# job starting thread
#
# ===================================================================

with open(os.environ["JOB_TEMPLATE_PATH"]) as reading:
    _job_creation_template_string = reading.read()
_job_creation_template = jinja2.Template(_job_creation_template_string, trim_blocks=True, lstrip_blocks=True)
mongo_template_input = {
    "uri": os.environ['MONGO_CONNECTION_URI'],
    "database": os.environ['MONGO_DATABASE']
}

@thread_helper.start_safe_thread
def _start_k8s_jobs_entry():
    batch_api = client.BatchV1Api()
    while True:
        job_run_id = k8s_queues.queue_start_job.get()
        if job_run_id is None:
            break
        job_run = dao_job_run.find_by_id(job_run_id)
        if job_run is None:
            logger.warning("not creating job because job_run_id is not found: %s", job_run_id)
            continue

        # find the next stage index
        next_stage_index = len(job_run["run_stages"])

        # start the next job
        next_stage_definition = job_run["job_definition"]["stages"][next_stage_index]
        next_stage = { "start": int(time.time()), "finished": 0, "state": "queue" }
        job_run["run_stages"].append(next_stage)
        dao_job_run.replace(job_run)

        try:
            template_result = _job_creation_template.render(
                job_run=job_run,
                namespace=job_namespace,
                job_definition=job_run["job_definition"],
                job_stage=next_stage_definition,
                stage_index=next_stage_index,
                stage=next_stage,
                mongo=mongo_template_input)
            logger.info(template_result)
            job_payload = yaml.load(io.StringIO(template_result))
        except:
            logging.exception("error during template for job run %s", job_run_id)
            next_stage["state"] = "finished_error"
            job_run["state"] = "finished_error"
            dao_job_run.replace(job_run)
            continue

        logger.info(job_payload)

        batch_api.create_namespaced_job(job_namespace, job_payload)
        logger.info("started next job => %s", job_run_id)
        k8s_queues.queue_start_job.task_done()

# ===================================================================
#
# job finished handler thread
#
# ===================================================================

@thread_helper.start_safe_thread
def _finish_k8s_jobs_entry():
    while True:
        job_run_event = k8s_queues.queue_job_finished_check.get()
        if job_run_event is None:
            break

        # first, lets find the job_run
        job_run = dao_job_run.find_by_id(job_run_event["job_run_id"])
        if job_run is None:
            logger.info("cannot finish job... unable to find job_run: %s", job_run_event)
            continue

        # sanity check
        if len(job_run["run_stages"]) - 1 != job_run_event["job_stage_index"]:
            logger.error("job stage index doesnt match job stage count: %s", job_run_event)
            job_run["state"] = "finished_error"
            dao_job_run.replace(job_run)
            continue

        # update the stage result
        job_run["run_stages"][-1]["state"] = "finished_success" if job_run_event["job_result_succeeded"] else "finished_error"

        # check if the pipeline failed... if it did, then save that and continue
        if job_run["run_stages"][-1]["state"] != "finished_success":
            logger.info("marking job as failed: %s", job_run_event)
            job_run["state"] = job_run["run_stages"][-1]["state"]
            dao_job_run.replace(job_run)
            continue

        # check if there are any more stages
        if len(job_run["run_stages"]) >= len(job_run["job_definition"]["stages"]):
            logger.info("marking job as success: %s", job_run_event)
            # we're actually finished with the entire job... and if we get here, that means
            # the entire job was a success
            job_run["state"] = "finished_success"
            dao_job_run.replace(job_run)
            continue

        logger.info("job stage a success: %s", job_run_event)
        k8s_queues.queue_start_job.put(job_run_event["job_run_id"])
        k8s_queues.queue_job_finished_check.task_done()
