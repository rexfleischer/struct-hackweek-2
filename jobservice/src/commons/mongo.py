from enum import unique

import logging
import pymongo
import pymongo.errors
import os
import atexit
from .model import event_validate
from .model import job_definition_validate
from .model import job_run_validate
from .model import job_run_attempt_validate

logger = logging.getLogger("commons.mongo")
connection_string = os.environ['MONGO_CONNECTION_URI']
database_name = os.environ['MONGO_DATABASE']

mongo_client = pymongo.MongoClient(connection_string)
mongo_database = mongo_client[database_name]

def mongo_do_indexing():
    get_job_definition_dao()._collection.create_index([("project", pymongo.ASCENDING), ("name", pymongo.ASCENDING)], unique=True)
    get_job_definition_dao()._collection.create_index("name")
    get_job_run_attempt_dao()._collection.create_index([("job_run_id", pymongo.ASCENDING), ("job_pod_id", pymongo.ASCENDING)], unique=True)

def shutdown_mongo():
    mongo_client.close()

atexit.register(shutdown_mongo)

def mongo_status_check():
    mongo_database.command("serverStatus")

def get_events_dao():
    return MongoDao("events", event_validate)

def get_job_definition_dao():
    return MongoDao("job_definition", job_definition_validate)

def get_job_run_dao():
    return MongoDao("job_run", job_run_validate)

def get_job_run_attempt_dao():
    return MongoDao("job_run_attempt", job_run_attempt_validate)

class MongoDao(object):
    def __init__(self, collection_name, model_validator):
        self._collection = mongo_database.get_collection(collection_name)
        self._model_validator = model_validator

    def insert(self, model):
        self._model_validator(model)
        try:
            self._collection.insert_one(model)
            return None
        except pymongo.errors.DuplicateKeyError:
            return "duplicate model"

    def upsert(self, model):
        self._model_validator(model)
        result = self._collection.replace_one({ "_id": model["_id"] }, model, upsert=True)
        was_insert = result.modified_count == 0
        return was_insert

    def replace(self, model):
        self._model_validator(model)
        result = self._collection.replace_one({ "_id": model["_id"] }, model)
        was_updated = result.modified_count == 1
        return was_updated

    def delete_by_id(self, model_id):
        result = self._collection.delete_one({ "_id": model_id })
        was_deleted = result.deleted_count == 1
        return was_deleted

    def find_by_id(self, model_id):
        return self._collection.find_one({ "_id": model_id })

    def find_by_ids(self, model_ids):
        return list(self._collection.find({ "_id": { "$in": model_ids } }))

    def search(self, query, projection=None, skip=None, limit=None, sort=None):
        cursor = self._collection.find(query, projection=projection)
        if sort is not None:
            cursor = cursor.sort(sort)
        if skip is not None:
            cursor = cursor.skip(skip)
        if limit is not None:
            cursor = cursor.limit(limit)
        return list(cursor)

    def search_one(self, query, projection=None, skip=0, sort=None):
        return self._collection.find_one(
            filter=query,
            projection=projection,
            skip=skip,
            sort=sort
        )
