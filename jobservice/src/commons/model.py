from jsonschema import validate
from copy import deepcopy
import uuid
import time
import os

# ===================================================================
#
# load all definitions from swagger
#
# ===================================================================

with open(os.environ["SWAGGER_HTTP_LOCATION"]) as swagger_yaml:
    import yaml

    swagger = yaml.safe_load(swagger_yaml.read())
    model_schemas = swagger["definitions"]
    # import pprint
    # pprint.pprint(swagger, width=200)

# ===================================================================
#
# event
#
# ===================================================================

event_schema = deepcopy(model_schemas["Event"])
event_schema["definitions"] = model_schemas

def event_validate(check):
    validate(check, event_schema)

def event_create(project, event_type, message, tags, details):
    result = {
        "_id": str(uuid.uuid4()),
        "project": project,
        "event_type": event_type,
        "event_time": int(time.time()),
        "message": message,
        "tags": tags,
        "details": details
    }
    validate(result, event_schema)
    return result

# ===================================================================
#
# job definition
#
# ===================================================================

job_definition_schema = deepcopy(model_schemas["JobDefinition"])
job_definition_schema["definitions"] = model_schemas

def job_definition_validate(check):
    validate(check, job_definition_schema)

def job_definition_create(project, name):
    """

    :param project: the name of the project
    :param name: the name of the job
    :return:
    """
    result = {
        "_id": str(uuid.uuid4()),
        "project": project,
        "name": name,
        "environment": { },
        "tags": [],
        "stages": []
    }
    validate(result, job_definition_schema)
    return result

# ===================================================================
#
# job stage run
#
# ===================================================================

job_run_attempt_schema = deepcopy(model_schemas["JobRunAttempt"])
job_run_attempt_schema["definitions"] = model_schemas

def job_run_attempt_validate(check):
    validate(check, job_run_attempt_schema)

def job_run_attempt_create(job_run_id, job_pod_id, stage_index):
    result = {
        "_id": str(uuid.uuid4()),
        "start": int(time.time()),
        "finished": 0,
        "state": "queue",
        "job_run_id": job_run_id,
        "job_pod_id": job_pod_id,
        "stage_index": stage_index,
        "logs": ""
    }
    validate(result, job_run_attempt_schema)
    return result

# ===================================================================
#
# job run
#
# ===================================================================

job_run_schema = deepcopy(model_schemas["JobRun"])
job_run_schema["definitions"] = model_schemas

def job_run_validate(check):
    validate(check, job_run_schema)

def job_run_create(job_definition, input_environment, tags):
    """

    :param job_definition: a complete copy of the job definition
    :param input_environment: the input environment variables
    :param tags: the added tags
    :return:
    """
    result = {
        "_id": str(uuid.uuid4()),
        "start": int(time.time()),
        "finished": 0,
        "state": "queue",
        "job_name": job_definition["name"],
        "job_definition_id": job_definition["_id"],
        "job_definition": job_definition,
        "run_stages": [],
        "input_environment": input_environment,
        "tags": tags,
    }
    validate(result, job_run_schema)
    return result
