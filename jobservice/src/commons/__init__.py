import logging

logger_format = "%(asctime)s.%(msecs)03d %(levelname)-5s %(module)s: %(message)s"
logging.basicConfig(level=logging.INFO, format=logger_format, datefmt="%H:%M:%S")