
job service built on kubernetes that allows the upload of docker images to a private repo
to be executed and reported on based on job configs.


### entire code base

```
rfleischer-l:struct-hackweek-2 rfleischer$ cloc . --exclude-dir=.idea
      80 text files.
      78 unique files.
      14 files ignored.

github.com/AlDanial/cloc v 1.76  T=1.08 s (63.7 files/s, 4546.3 lines/s)
--------------------------------------------------------------------------------
Language                      files          blank        comment           code
--------------------------------------------------------------------------------
Python                           24            255            134           1389
YAML                              9             28             53           1360
Bourne Again Shell               28            153             29            834
JSON                              4              0              0            625
Markdown                          2             10              0             36
Dockerfile                        2              6              0             10
--------------------------------------------------------------------------------
SUM:                             69            452            216           4254
--------------------------------------------------------------------------------
```


### working code base

```
rfleischer-l:struct-hackweek-2 rfleischer$ cloc ./jobservice/src/
      16 text files.
      16 unique files.
       1 file ignored.

github.com/AlDanial/cloc v 1.76  T=1.04 s (15.4 files/s, 836.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          16            133             73            661
-------------------------------------------------------------------------------
SUM:                            16            133             73            661
-------------------------------------------------------------------------------
```
