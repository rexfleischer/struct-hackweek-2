#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
docker build $THIS_DIR -t starlord.inscloudgate.net/rfleischer/canvas-lms:master
docker push starlord.inscloudgate.net/rfleischer/canvas-lms:master