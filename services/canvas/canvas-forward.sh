#!/usr/bin/env bash


export POD_NAME=$(kubectl get pods --namespace canvas -l "app.kubernetes.io/name=canvas,app.kubernetes.io/instance=canvas" -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward $POD_NAME 8080:3000 -n canvas
