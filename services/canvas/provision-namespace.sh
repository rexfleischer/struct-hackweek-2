#!/usr/bin/env bash

source $JOB_SERVICE_LIB_BASH_INIT
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

kube_namespace_ensure canvas

# docker login -u rfleischer -p eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3MiOlt7InR5cGUiOiJyZXBvc2l0b3J5IiwibmFtZSI6IioiLCJhY3Rpb25zIjpbInB1bGwiXX0seyJ0eXBlIjoicmVnaXN0cnkiLCJuYW1lIjoiY2F0YWxvZyIsImFjdGlvbnMiOlsic2VhcmNoIl19LHsidHlwZSI6InJlcG9zaXRvcnkiLCJuYW1lIjoicmZsZWlzY2hlci8qIiwiYWN0aW9ucyI6WyJwdWxsIiwicHVzaCIsImRlbGV0ZSJdfV0sImF1ZCI6InN0YXJsb3JkLmluc2Nsb3VkZ2F0ZS5uZXQiLCJleHAiOjE1NzEzMzgzMTksImlhdCI6MTU3MDczMzUxOSwiaXNzIjoiU0FNTCIsInN1YiI6InJmbGVpc2NoZXIifQ.dbB9GSZ8zcREyhnCpPwH-AyivmvxcubztF1mSZlUbR-cf30aov5CTIc5HN0KM4gYuDy6nnKn_3Z2lCxOHo0nvw starlord.inscloudgate.net
echo "creating starlord pull secret"
NAME=starlord NAMESPACE=canvas SERVER=starlord.inscloudgate.net USERNAME=rfleischer EMAIL=rfleischer@instructure.com PASSWORD=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3MiOlt7InR5cGUiOiJyZXBvc2l0b3J5IiwibmFtZSI6IioiLCJhY3Rpb25zIjpbInB1bGwiXX0seyJ0eXBlIjoicmVnaXN0cnkiLCJuYW1lIjoiY2F0YWxvZyIsImFjdGlvbnMiOlsic2VhcmNoIl19LHsidHlwZSI6InJlcG9zaXRvcnkiLCJuYW1lIjoicmZsZWlzY2hlci8qIiwiYWN0aW9ucyI6WyJwdWxsIiwicHVzaCIsImRlbGV0ZSJdfV0sImF1ZCI6InN0YXJsb3JkLmluc2Nsb3VkZ2F0ZS5uZXQiLCJleHAiOjE1NzEzMzgzMTksImlhdCI6MTU3MDczMzUxOSwiaXNzIjoiU0FNTCIsInN1YiI6InJmbGVpc2NoZXIifQ.dbB9GSZ8zcREyhnCpPwH-AyivmvxcubztF1mSZlUbR-cf30aov5CTIc5HN0KM4gYuDy6nnKn_3Z2lCxOHo0nvw \
$DIR/../provision-docker-secret

echo "creating internal pull secret"
NAME=internal-docker-registry NAMESPACE=canvas SERVER=127.0.0.1:30080 USERNAME=default-user PASSWORD=default-pass EMAIL=default@defaultnot.blah \
$DIR/../provision-docker-secret

echo "patching default service account for pull secret: canvas"
kubectl patch serviceaccount default -n canvas -p '{"imagePullSecrets":[{"name":"starlord"},{"name":"internal-docker-registry"}]}'
