#!/usr/bin/env bash

source $JOB_SERVICE_LIB_BASH_INIT
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

k8s-script --image postgres:9.5 --pod-name postgres-check --namespace $NAMESPACE -- bin/bash <<-SCRIPT
set -ex
pg_isready -h $NAME-postgresql.$NAMESPACE.svc.cluster.local
SCRIPT
